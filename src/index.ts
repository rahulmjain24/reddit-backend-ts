import express from "express";
import cors from 'cors'

import { port } from "./config/config"
import { getSeries, getById, updateById, deleteById, createNewPost } from "./queries/query"

const app = express()

app.use(cors())
app.use(express.json())

app.get('/redditdata', getSeries)

app.get('/redditdata/:id', getById)

app.post('/redditdata', createNewPost)

app.patch('/redditdata/:id', updateById)

app.delete('/redditdata/:id', deleteById)

app.get('/', (req, res) => {
    res.send('<a href="/redditdata">Data<a>')
})

app.listen(port, () => {
    console.log('listening on port '+ port)
})
